site_name: Documentation
site_description: Central documentation for the `stack` group
site_url: https://stack7013932.gitlab.io/documentation/
site_dir: public
repo_url: https://gitlab.com/stack7013932/documentation
repo_name: stack/documentation
edit_uri: edit/main/docs/

theme:
    logo: assets/logo.png
    palette:
      primary: black
      accent: teal
    name: 'material'
    font: false # Set to false for GDPR reasons, see https://squidfunk.github.io/mkdocs-material/setup/changing-the-fonts/#autoloading
    language: en

    features:
        - navigation.instant
        - navigation.indexes
        - navigation.tracking
        - navigation.path
        - navigation.top
        - navigation.prune
        - navigation.tabs
        - navigation.tabs.sticky
        - navigation.expand
        - navigation.footer
        - search.suggest
        - search.highlight
        - search.share
        - toc.follow
        - content.tooltips
        - content.action.edit
    icon:
      repo: fontawesome/brands/gitlab

plugins:
  - search
  - tags:
      tags_file: tags.md
  - git-revision-date-localized:
      enable_creation_date: true
  - termynal

# Extensions
markdown_extensions:
  - footnotes
  - nl2br
  - abbr
  - attr_list
  - sane_lists
  - meta
  - smarty
  - tables
  - mdx_breakless_lists
  - def_list
  - tables
  - pymdownx.arithmatex:
      generic: true
  - pymdownx.details
  - pymdownx.magiclink
  - pymdownx.critic
  - pymdownx.caret
  - pymdownx.keys
  - pymdownx.mark
  - pymdownx.tilde
  - pymdownx.highlight:
      use_pygments: true
      anchor_linenums: true
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
  - admonition
  - toc:
      permalink: true
  - pymdownx.inlinehilite
  - pymdownx.snippets
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:mermaid2.fence_mermaid

extra_css:
  - stylesheets/extra.css
extra_javascript:
  - scripts/extra.js

nav:
  - Home : index.md
  - Tutorials:
    - tutorials/index.md
    - Python:
      - Building a documentation website with mkdocs: tutorials/build-documentation-website-with-mkdocs.md
      - Creation of a cli tool from a python package using Typer: tutorials/create-cli-from-python-package.md
      - Developing a python package: tutorials/develop-a-python-package.md
  - How-to guides:
    - Hatch:
      - How to add a pypi package to `pyproject.toml` using hatch: how-to-guides/hatch/add-package-with-hatch.md
      - How to create a virtualenv using hatch: how-to-guides/hatch/create-virtualenv-with-hatch.md
  - Explanations:
    - Documentation structure: explanation/diataxis.md
    - Documentation server: explanation/mkdocs.md
    - Hatch: explanation/hatch.md
    - Pre-commit: explanation/pre-commit.md
  - Tags: tags.md
