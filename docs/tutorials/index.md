# List of tutorials

## Python tutorials

### Poetry

- [Develop a python package](develop-a-python-package.md)

### Typer
- [Create a cli tool from a python package using Typer](create-cli-from-python-package.md)

### Mkdocs
- [Build a documentation website with mkdocs](build-documentation-website-with-mkdocs.md)

<!-- termynal {title: powershell, buttons: windows}-->

```
$ this is a command
```

<br/>

<!-- termynal -->

```
$ show progress
---> 100%
Done!
```