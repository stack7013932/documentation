# Why should I declare specific values in `appsettings.json` or in `pyproject.toml` ?

Both `appsettings.json` and `pyproject.toml` are configuration files for a specific project (written in `C#` or `python`).

Those two files tend to gather the configuration for several tools :
- application config
- packages configuration (logging, tests)

And we might want to use it as well to declare some values for Continuous Integration and Deployment :
- server name that will host the program
- scheduling values for scheduled tasks etc

Some rules:
- always declare the program-specific config appart:

```json
// appsettings.json
{
    "app" : {
        "mailAddress": "test@mail.com"
        // all program-related config values go in here
    }
}
```

```toml
# pyproject.toml
[app]
mail-address = "test@mail.com"
```

Please refer to `json` and `toml` format to declare variables in a valid manner.

## How to read from my config file ?

TODO

## Schemas
