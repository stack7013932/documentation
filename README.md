# Documentation

This repo aims at explaining all the concepts and ideas used in the [`stack`](https://gitlab.com/stack7013932) group.

## First steps

<!-- termynal -->

```
$ pip install hatch
$ hatch env create docs
$ hatch run docs:serve
```
